const produtos = [
    { nome: 'Notebook', preco: 2499, fragil: true},
    {nome: 'ipad pro', preco: 4199, fragil: true},
    {nome: 'Copo de vidro', preco: 12.49, fragil: true},
    {nome: 'Copo de Plástico', preco: 18.99, fragil: false}
]

//console.log(produtos.filter(function(p){
  //  return true
//}))

const isBarato = function(produtos){
    return produtos.preco <=20 && produtos.fragil == false
}

const isCaro = produto => produto.preco >=500
const isFragil = produto => produto.fragil

//console.log(produtos.filter(isCaro).filter(isFragil))
console.log(produtos.filter(isBarato))

