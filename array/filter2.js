Array.prototype.filter2 = function(callback) {
    const newArray = []
    for(let i =0; i < this.length; i++){
        if(callback(this[i], i, this)){
            newArray.push(this[i])
        }
    }
}

const produtos = [
    { nome: 'Notebook', preco: 2499, fragil: true},
    {nome: 'ipad pro', preco: 4199, fragil: true},
    {nome: 'Copo de vidro', preco: 12.49, fragil: true},
    {nome: 'Copo de Plástico', preco: 18.99, fragil: false}
]


const Caro = produto => produto.preco <= 20
const Fragil = produto => produto.fragil

//console.log(produtos.filter2(Caro).filter2(fragil))

const soma = function (a,b){
    return a+b
}
console.log(soma(1,1))

const mult = (a,b) =>  console.log(a*b)
  
console.log(mult(2,10))

/*const Pessoas = p1= {nome:'ANA',idade:15},    OBJETO
                p2={nome:'BIA',idade:17},
                p3={nome:'LIA',idade:14},
                p4={nome:'BEATRIZ',idade:13},
                p5={nome:'JHENNYFER',idade:21}
*/
const Pessoas = [{nome:'ANA',idade:15},    //Array objetos
                {nome:'BIA',idade:17},
                {nome:'LIA',idade:14},
                {nome:'BEATRIZ',idade:13},
                {nome:'JHENNYFER',idade:21}]      

const novinhas = p => p.idade <15
console.log(Pessoas.filter(novinhas))
//console.log(p1)


