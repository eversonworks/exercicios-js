//Funão sem retorno
function imprimirSoma(a, b){
    console.log(a+b)
}
imprimirSoma(2,3)
imprimirSoma('everson',","+'jheny')
imprimirSoma(2,12,23,243)

//Função com retorno com os parametros tratados se eu não tratar os parametros da NaN
function soma(a =10, b = 10) {
    return a + b
}
//soma(2,3) nao imprime pq tem retorno
console.log(soma(2))
console.log(soma())