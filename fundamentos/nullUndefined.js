let valor //não inicializa pq nao atribui nada para esssa variavel valor padrao undefined
console.log(valor)

valor = null // ausência de valor 
console.log(valor)
//console.log(valor.toString())//ERRO!

const produto = {}
console.log(produto.nome)
console.log(produto)

produto.preco = 3.50
console.log(produto.preco.toFixed(2))

produto.preco = undefined //evitar atribuir undefined
console.log(!produto.preco)
console.log(produto)