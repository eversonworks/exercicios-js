function rand({ min=0, max=1000}){
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}
const obj = { max:50, min:40}
console.log(rand(obj))
console.log(rand({min:955})) //passando apenas min ele pega o max da funcao
console.log(rand({}))  //passando objeto vazio
//console.log(rand()) não passando objeto