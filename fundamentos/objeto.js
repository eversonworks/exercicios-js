const prod1 = {}
prod1.nome = 'Celular Ultra Mega'
prod1.preco = 4998.90

console.log(prod1)
console.log("O preço do produto 1 é :R$".concat(prod1.preco.toFixed(2)))

const prod2 = {
    nome: 'Camisa Polo',
    preco: 79.90,
    tipo: 'Masculino',
    obj: {
        blabla: 1,

    }

}
console.log(prod2)