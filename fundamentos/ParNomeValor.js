const saudacao = 'Olá' //contexto léxico 1
console.log(saudacao)

function exec(){
    const saudacao = 'Falaaa'  // contexto léxico 2
    return saudacao
}
console.log(exec())

const cliente = {
    nome: 'Pedro',
    idade: 32,
    peso: 90,
    edereco: {
        logradouro: 'Rua Muito Legal',
        numero: 123,
        UF: 'DF'
    }
}
console.log(saudacao)
console.log(cliente)
