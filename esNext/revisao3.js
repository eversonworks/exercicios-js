// ES8: Object.values//Object.entries  1 pega os valores do objeto .. entries me da chave,valor em array
const obj = { a:1, b:2 , c: 3}
console.log(Object.keys(obj))
console.log(Object.values(obj))
console.log(Object.entries(obj))

//Melhorias na Notação Literal de Objeto

const nome = 'Carla'
const pessoa = {
    nome,
    ola() {           //não precisa mais atribuir nome pra function
       return 'Oi gente!'
    }
}
console.log(`O nome da pessoa é ${pessoa.nome}e ela fala ${pessoa.ola()}`)

// Class
class Animal {}
class Cachorro extends Animal {
    falar() {
        return 'Au au!'
    }
}

console.log(new Cachorro().falar())