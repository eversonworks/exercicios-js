{
    var a = 2
    let b = 3
    console.log(b)
}

console.log(a)

//Template String
const produto = 'iPad'
console.log(`${produto} é caro!`)

//Destruturing 
const [l,e, ...tras] = "Cod3r"
console.log(l,e,tras)

const [x, ,y] = [1,2,3]
console.log(x,y)

const {idade:i,nome:n} = {nome:'Ana', idade:9}
console.log(i,n)

const pessoas = ['ana','bia','lia','maria','orlanda']
const [p1,,p3] = pessoas
console.log(p1,p3)
