//Arrow Function V:ES6 2015
// E SÃO funcoes anonimas
const soma = (a,b) => a + b  //sem o corpo da função o return é chamado implicidamente
console.log(soma(2,3))

//Arrow function
const lexico1 = () => console.log(this === exports)
const lexico2 = lexico1.bind({})
lexico1()
lexico2()

//parametro default
function log(texto = 'Node'){
    console.log(texto)
}
log()
log('Sou mais forte!')

// operador rest //esses ... é o operador rest que agrupa >>>>>>>>>>>>
// >>>meus parametros em um array e dai eu posso trabalhar com esse array dentro da minha funcao
function total(...numeros) {
    let total = 0
    numeros.forEach(n => total += n)
    return total
}
console.log(total(2,3,4,5))
