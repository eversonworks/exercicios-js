// Factory simples

function criarPessoa(n,s) {
    return {
        nome: n,
        sobrenome: s
    }
}

console.log(criarPessoa('Julia','Meeiros'))
console.log(criarPessoa('marcos','Meeiros'))
console.log(criarPessoa('Everson','Ortega'))

