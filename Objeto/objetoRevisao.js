//coleção dinâmica de pares chave / valor
const produto = new Object
produto.nome = 'Cadeira'
produto['Marca do produto'] = 'Generica'
produto.preco = 220
console.log(produto)
delete produto.preco
delete produto['Marca do produto']
console.log(produto)

const carro = {
    modelo: 'A4',
    valor: 89000,
    proprietario: {
        nome:'Raul',
        idade: 56,
        endereco: {
            logradouro:'Rua ABC',
            numero: 123
        }
    },
    condutores: [{
        nome: 'Junior',
        idade: 19
    },{
        nome: 'Ana',
        idade: 42
    }],
    calcularValorSeguro: function() {
        //...
    }
}

carro.proprietario.endereco.numero = 1000
//carro['proprietario...]o['proprietario...]
console.log(carro.condutores)
delete carro.calcularValorSeguro
console.log(carro)
console.log(carro.calcularValorSeguro)// eu deletei o metodo calcularvalor..na linha 37 e então
//não tem problema eu tentar acessar um atributo que ainda não existe em carro pois carro existe
//console.log(carro.calcularValorSeguro.length)// o problema é eu tenta acessa a propriedade de algo 
//indefinido ou nulo vai gerar um erro
console.log(carro.condutores.length)